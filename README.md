This small repo contains everything needed to create a docker-based test setup for load balancing environment.

To launch it, use the deploy script.

To view stats, use `docker stats`.

Load balancing methods: https://docs.nginx.com/nginx/admin-guide/load-balancer/http-load-balancer/#choosing-a-load-balancing-method