#!/bin/bash

# build dummy service
sudo docker build -t dummy_service service

# run dummy service instances
NINSTANCES=2
nginx_conf=""
for (( c=0; c<$NINSTANCES; c++ ))
do
    let port=8080+$c
    # outside port is left open for external tests.
    sudo docker run -d -p $port:8080 --name dummy_service_$c dummy_service

    # wait for container to be up
    until [ "`docker inspect -f {{.State.Running}} dummy_service_$c`"=="true" ]; do
        sleep 0.1;
    done;

    # retrieve its IP, pupulate load balancer conf with it
    ip=$(docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' dummy_service_$c)
    nginx_conf="${nginx_conf}    server $ip:8080;\n"
done

# create LB's conf, image and then launch it
sed "s/{servers}/$nginx_conf/g" load_balancer/nginx_template.conf > load_balancer/default.conf
docker run -d -p 8070:80 --name load_balancer -v "$(pwd)"/load_balancer/default.conf:/etc/nginx/conf.d/default.conf nginx

read -p "Press enter to launch test"

# run load injector
k6 run load_injector/script.js

read -p "Press enter to clean up"

sudo docker rm --force load_balancer
for (( c=0; c<$NINSTANCES; c++ ))
do
    sudo docker rm --force dummy_service_$c
done


sudo docker rmi dummy_service
