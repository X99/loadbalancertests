import http from 'k6/http';
import { check, sleep } from 'k6';

export let options = {
    vus: 50,
    duration: '5s',
};

function makeword(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

export default function () {
    let word = makeword(10);
    let reversed = word.split("").reverse().join("");
    let res = http.get('http://0.0.0.0:8070/' + word);
    check(res, { 'status was 200': (r) => r.status == 200 });
    check(res, { 'body was reversed': (r) => r.body == reversed });
}